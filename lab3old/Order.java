public class Order{
	public static final int MAX_NUMBERS_ORDERED = 10;
	public  DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	
	public void setQty(int i){
		if (i > MAX_NUMBERS_ORDERED){
			System.out.println("Invalid number!");
			return;
		}
		this.qtyOrdered = i; 
	}
	public int getQty(){
		return qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc){
		if (qtyOrdered < MAX_NUMBERS_ORDERED){
			itemsOrdered[qtyOrdered] = disc;
			System.out.println("The disc has been added!");
			qtyOrdered += 1;
		}
		else{
			System.out.println("The order is full, Failed to add the disc!");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc){
		int index = 0;
		int check = 0;
		for (int i = 0;i<qtyOrdered;i++){
			if (itemsOrdered[i] == disc){
				index = i;
				check = 1;
			}
		}
		if (check == 0){
			System.out.println("Disc does not exist in the order!");
			return;
		}
		for (int i = index; i<qtyOrdered - 1;i++){
			itemsOrdered[i] = itemsOrdered[i+1];
		}
		qtyOrdered -= 1;
	}
	public float totalCost(){
		float total = 0;
		for(int i = 0; i < qtyOrdered; i++){
			total += itemsOrdered[i].getCost();
		}
		return total;
	}
}
