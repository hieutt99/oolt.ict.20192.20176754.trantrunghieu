package vbox;


import javafx.application.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

public class VBoxDemo extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		VBox root = new VBox();
		
		root.setSpacing(10);
		root.setPadding(new Insets(15, 20, 10, 10));
		
//		Button 1
		Button button1 = new Button("Button 1");
		root.getChildren().add(button1);
		
//		Button 2
		Button button2 = new Button("Button 2");
		button2.setPrefSize(100,100);
		root.getChildren().add(button2);
//		Text Field 
		TextField textField = new TextField("Text Field");
		textField.setPrefWidth(110);
		
		root.getChildren().add(textField);
		
//		CheckBox
		CheckBox checkBox = new CheckBox("Check Box");
		root.getChildren().add(checkBox);
		
//		RadioButton
		RadioButton radioButton = new RadioButton("Radio Button");
		root.getChildren().add(radioButton);
		
		Scene scene = new Scene(root, 550, 250);
		
		primaryStage.setTitle("VBox Layout Demo");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
}
