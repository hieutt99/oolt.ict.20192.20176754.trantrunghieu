package application;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.control.*;

public class MyController implements Initializable {
	
	@FXML
	private Button myButton;
	@FXML
	private TextField myTextField;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	
	public void showDateTime(ActionEvent event) {
		System.out.println("Button Clicked");
		
		Date now = new Date();
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
		
//		Model Data
		String dateTimeString =df.format(now);
		myTextField.setText(dateTimeString);
	}
}







