package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>();
//	===============================================================
	
	public CompactDisc(){
	}
	public CompactDisc(int id) {
		super(id);
	}
	public CompactDisc(String title) {
		super(title);
	}
	public CompactDisc(String title, String category) {
		super(title, category);
	}
	public CompactDisc(int id, String title, String category) {
		super(id, title, category);
	}
	public CompactDisc(int id, String title, String category, float cost) {
		super(id, title, category, cost);
	}
//	===============================================================
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public void addTrack(Track t) {
		boolean check = tracks.contains(t);
		if (check) {
			System.out.println("Element existed!");
			return;
		}
		else {
			tracks.add(t);
			System.out.println("Track added!");
		}
	}
	
	public void removeTrack(Track t) {
		boolean check = tracks.contains(t);
		if (check) {
			int pos = tracks.indexOf(t);
			tracks.remove(pos);
			System.out.println("Removed element");
		}
		else {
			System.out.println("Element does not exist in the tracklist");
			return;
		}
	}
	
	public int getLength() {
		length = 0;
		for (Track iter: tracks) {
			length += iter.getLength();
		}
		return length;
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		for (Track track: tracks) {
			track.play();
		}
	}
}
