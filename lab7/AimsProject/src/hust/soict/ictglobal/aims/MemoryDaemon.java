package hust.soict.ictglobal.aims;

import java.lang.Runtime;

public class MemoryDaemon implements java.lang.Runnable{
	long memoryUsed;
	
	public MemoryDaemon() {
		memoryUsed = 0;
	}
	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;
		
		while (true) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != memoryUsed) {
				System.out.println("***************** - Memory used = "+used);
				memoryUsed = used;
			}
		}
	}
}
