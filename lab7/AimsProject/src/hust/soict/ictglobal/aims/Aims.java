package hust.soict.ictglobal.aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.media.*;
import hust.soict.ictglobal.aims.order.Order;

public class Aims extends Thread{
	private static int id = 0;
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	public static void showType() {
		System.out.println("Choose type of item : ");
		System.out.println("1. Book");
		System.out.println("2. Compact Disc");
		System.out.println("3. Digital Video Disc");
	}
//	============================================================================
	private static void addBook(Order order) {
//		Book newBook = new Book();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println("Add Book : ");
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Input title : ");
		String title = scanner.nextLine();
		
		System.out.print("Input category : ");
		String category = scanner.nextLine();
		
		System.out.print("Input cost : ");
		float cost = scanner.nextFloat();
		
		int check = order.checkByTitle(title);
		if (check != -1) {
			Book newBook = new Book(id, title, category, cost);
			order.addMedia(newBook);
			id++;
		}
		else {
			System.out.println("Title existed in the order!");
		}
		
		scanner.close();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	private static void addCD(Order order) {
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println("Add CD : ");
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Input title : ");
		String title = scanner.nextLine();
		
		System.out.print("Input category");
		String category = scanner.nextLine();
		
		System.out.print("Input cost : ");
		float cost = scanner.nextFloat();
		
		int check = order.checkByTitle(title);
		if (check != -1) {
			CompactDisc newCD = new CompactDisc(id, title, category, cost);
			order.addMedia(newCD);
			id++;
			
			int temp;
			System.out.print("Do you want to play (1/0): ");
			temp = scanner.nextInt();
			if (temp == 1) {
				newCD.play();
			}
		}
		else {
			System.out.println("Title existed in the order!");
		}
		
				
		scanner.close();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	private static void addDVD(Order order) {
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println("Add DVD : ");
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Input title : ");
		String title = scanner.nextLine();
		
		System.out.print("Input category");
		String category = scanner.nextLine();
		
		System.out.print("Input cost : ");
		float cost = scanner.nextFloat();
		
		int check = order.checkByTitle(title);
		if (check != -1) {
			DigitalVideoDisc newDVD = new DigitalVideoDisc(id, title, category, cost);
			order.addMedia(newDVD);
			id++;
			
			int temp;
			System.out.print("Do you want to play (1/0): ");
			temp = scanner.nextInt();
			if (temp == 1) {
				newDVD.play();
			}
		}
		else {
			System.out.println("Title existed in the order!");
		}
		scanner.close();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
//	===============================================================
	public static void main(String[] args) {
		Thread thread = new Thread(new MemoryDaemon());
		thread.setDaemon(true);
		thread.start();
		
		int choice;
		Order newOrder = null;
		
//		Book sampleBook = new Book(1, "Harry Potter", "Fiction", (float)20.0);
//		DigitalVideoDisc sampleDVD = new DigitalVideoDisc(2, "Star Wars", "Science Fiction", (float)30.0);
//		CompactDisc sampleCD = new CompactDisc(3, "Plastic Love", "Funk Pop", (float) 50.0);
		
		Scanner scan = new Scanner(System.in);
		do {
			showMenu();
			System.out.print("Input your choice \\> ");
			choice = scan.nextInt();
			switch(choice) {
				case 1:
						newOrder = new Order();
						break;
				
				case 2:
						System.out.print("Add item to Order : ");
						showType();
						System.out.print("Input type of item : ");
						int temp = scan.nextInt();
						if (temp == 1) {
//							Book newBook = new Book();
//							newBook = sampleBook;
//							newOrder.addMedia(newBook);
							addBook(newOrder);
						}
						else if (temp == 2) {
//							CompactDisc newCD = new CompactDisc();
//							newCD = sampleCD;
//							newOrder.addMedia(newCD);
							addCD(newOrder);
							
//							System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//							System.out.print("Do you want to play it ? (1/0) : ");
//							temp = scan.nextInt();
//							if (temp == 1) {
//								newCD.play();
//							}
//							System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
						}
						else if (temp == 3) {
//							DigitalVideoDisc newDVD = new DigitalVideoDisc();
//							newDVD = sampleDVD;
//							newOrder.addMedia(newDVD);
							addDVD(newOrder);
							
//							System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//							System.out.print("Do you want to play it ? (1/0) : ");
//							temp = scan.nextInt();
//							if (temp == 1) {
//								newDVD.play();
//							}
//							System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
						}
						else {
							System.out.println("Invalid choice");
						}
						break;
				
				
				case 3:	
						System.out.print("Delete item by id : ");
						System.out.print("Input id : ");
						int id = scan.nextInt();
						newOrder.removeMedia(id);
						break;
				case 4:
						newOrder.printList();
						break;
				case 0:
						System.out.println("Quitting!");
						break;
				default:
					System.out.println("Invalid input !");
					break;
					
			}
		}while(choice != 0);
		
		scan.close();
		return;
	}
}
















