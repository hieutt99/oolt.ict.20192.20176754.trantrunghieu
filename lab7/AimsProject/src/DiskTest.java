import hust.soict.ictglobal.aims.media.*;
import hust.soict.ictglobal.aims.order.*;

//import java.util.*;
public class DiskTest {
	public static void main(String[] args) {
		Order anOrder = new Order();
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
//		anOrder.addDigitalVideoDisc(dvd1);
		anOrder.addMedia(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
//		anOrder.addDigitalVideoDisc(dvd2);
		anOrder.addMedia(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
//		anOrder.addDigitalVideoDisc(dvd3);
		anOrder.addMedia(dvd3);

		System.out.print("Total Cost is : ");
		System.out.println(anOrder.totalCost());
		System.out.print("Number of elements : ");
//		System.out.println(anOrder.getQty());
		
		System.out.println("Test remove elements: 1");
//		anOrder.removeDigitalVideoDisc(dvd1);
		anOrder.removeMedia(dvd1);
//		System.out.print("Number of elements left : ");
//		System.out.println(anOrder.getQty());
		System.out.println("Check title : ");
//		for (int i = 0; i< anOrder.getQty();i++){
//			System.out.println(anOrder.itemsOrdered[i].getTitle());
//		}
		anOrder.printList();
		
//		System.out.println("Test search disk : ");
//		Scanner scan = new Scanner(System.in);
//		System.out.print("Input : ");
//		String s = scan.nextLine();
//		scan.close();
//		anOrder.testSearch(s);
		
//		System.out.println("Test lucky :");
//		DigitalVideoDisc lucky = anOrder.getALuckyItem();
//		System.out.println("Lucky one : "+lucky.getTitle());
//		System.out.println("Test print : ");
//		anOrder.printOrder();
	}
}
