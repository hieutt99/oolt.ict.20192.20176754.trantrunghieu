package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Book extends Media implements Comparable {
	private String content;
	private List<String> contentTokens;
	private Map<String, Integer> wordFrequency;

	private List<String> authors = new ArrayList<>();

	// -------------------- constructor --------------

	public Book() {
	}

	public Book(int id) {
		super(id);
	}

	public Book(String title, String category) {
		super(title, category);
	}

	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
		// TODO: check author condition
	}

	public Book(String title, String category, List<String> authors, float cost) {
		super(title, category, cost);
		this.authors = authors;
		// TODO: check author condition
	}

	public Book(int id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		this.authors = authors;
		// TODO: check author condition
	}

	// --------------------------------- getter setter ------------------------

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}

	public List<String> getContentTokens() {
		return contentTokens;
	}

	public void setContentTokens(List<String> contentTokens) {
		this.contentTokens = contentTokens;
	}

	public Map<String, Integer> getWordFrequency() {
		return wordFrequency;
	}

	public void setWordFrequency(Map<String, Integer> wordFrequency) {
		this.wordFrequency = wordFrequency;
	}

	// --------------------- service ---------------------------------

	public void addAuthor(String authorName) {
		if (!authors.contains(authorName)) {
			List<String> res = this.getAuthors();
			res.add(authorName);
			this.setAuthors(res);
		} else
			System.out.println("Author's name already in list");
	}

	public void removeAuthor(String authorName) {
		if (authors.contains(authorName))
			this.setAuthors(
					this.getAuthors().stream().filter(name -> !name.equals(authorName)).collect(Collectors.toList()));
		else
			System.out.println("Author's name is not in list");
	}

	public void processContent() {
		String[] words = getContent().split("\\s+");
		List<String> tokens = Arrays.asList(words).stream().map(i -> i.replaceAll("[^\\w]", ""))
				.collect(Collectors.toList());
		tokens.sort(String::compareToIgnoreCase);

		setContentTokens(tokens);

		Map<String, Integer> counts = new HashMap<>();
		tokens.forEach(e -> counts.put(e, Collections.frequency(tokens, e)));
		setWordFrequency(counts);
	}

	@Override
	public String toString() {
		return "Book: title= " + getTitle() + ", cost = $" + getCost();
	}
	
	public String toStringTest() {
		return "Book: title= " + getTitle() + ", cost = $" + getCost() + ", contentLength = " + getContentTokens().size()
				+ "\nToken list: " + getContentTokens() + 
				"\nWord Frequency: " + getWordFrequency();
	}

	@Override
	public int compareTo(Object o) {
		Book b = (Book) o;
		return getTitle().compareTo(b.getTitle());
	}

}
