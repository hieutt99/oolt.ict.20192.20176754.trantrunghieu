package hust.soict.globalict.test.disc;

import java.util.ArrayList;
import java.util.List;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {
	public static void main(String[] args) {
		
		// initialize a new order
		Order order = new Order();
		List<Media> orderList = new ArrayList<>();
		orderList.add(new DigitalVideoDisc("The Lion Kingz", "Animation", "Roger Allers", 86, 29.95f));
		orderList.add(new DigitalVideoDisc("Star Warsz", "Science Fiction", "George Lucas", 124, 21.95f));
		orderList.add(new DigitalVideoDisc("Alladinz", "Animation", "John Musker", 90, 98.99f));
		orderList.add(new DigitalVideoDisc("Harry but not Potter", "Magic", "Hello World", 150, 49.99f));
		order.addMedia(orderList);
		
		// service
		order.printOrder();
		order.printOrderWithLuckyItem();
		
		// search
		System.out.println(((DigitalVideoDisc) orderList.get(3)).search("hArrY poTTer"));
	}
}
