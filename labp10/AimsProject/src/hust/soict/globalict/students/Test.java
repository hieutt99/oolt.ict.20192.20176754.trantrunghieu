package hust.soict.globalict.students;

import java.util.Scanner;

public class Test {
	public static void main(String[] args) throws IllegalGPAException, IllegalDobException {
		Scanner sc = new Scanner(System.in);
		
		Student student = new Student();

		System.out.print("Input studentID: ");
		int studentID = Integer.parseInt(sc.nextLine());
		student.setStudentID(studentID);

		System.out.print("Input studentName: ");
		String studentName = sc.nextLine();
		student.setStudentName(studentName);

		System.out.print("Input birthday (dd/mm/yyyy): ");
		String birthday = sc.nextLine();
		student.setDob(birthday);

		System.out.print("Input GPA: ");
		float gpa = sc.nextFloat();
		student.setGpa(gpa);
		
		System.out.println(student);
	}
}
