import java.time.*;
public class Order{
	public static final int MAX_NUMBERS_ORDERED = 10;
	public  DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	public String dateTime = new String();
	
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders = 0;
	
	public Order() {
		if (nbOrders < MAX_LIMITED_ORDERS) {
			nbOrders += 1;
		}
		else {
			System.out.println("Unable to create more order(s).");
			throw new IllegalArgumentException();
		}
		dateTime = LocalDate.now().toString().concat(" ").concat(LocalTime.now().toString());
	}
	public void printOrder() {
		System.out.println("*********************Order*************************");
		System.out.format("Date : %s\n", dateTime);
		System.out.println("Ordered Item : ");
		float total = 0;
		for (int i = 0; i<qtyOrdered;i++) {
			System.out.format("%d. DVD - %15s - %15s - %15s - %5s : %.3f $.\n", i+1, itemsOrdered[i].getTitle(),
					itemsOrdered[i].getCategory(), itemsOrdered[i].getDirector(),itemsOrdered[i].getLength(), 
					itemsOrdered[i].getCost());
			total += itemsOrdered[i].getCost();
		}
		System.out.println("Total cost : "+total+" $");
		System.out.println("***************************************************");
	}
	public void setQty(int i){
		if (i > MAX_NUMBERS_ORDERED){
			System.out.println("Invalid number!");
			return;
		}
		this.qtyOrdered = i; 
	}
	public void getDateTime() {
		System.out.println("Date created : "+dateTime);
	}
	public int getQty(){
		return qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc){
		if (qtyOrdered < MAX_NUMBERS_ORDERED){
			itemsOrdered[qtyOrdered] = disc;
			System.out.println("The disc has been added!");
			qtyOrdered += 1;
		}
		else{
			System.out.println("The order is full, Failed to add the disc!");
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		int temp = dvdList.length;
		System.out.println(temp);
		if (qtyOrdered + temp <= MAX_NUMBERS_ORDERED) {
			for (int i = 0;i<temp;i++) {
				itemsOrdered[qtyOrdered + i] = dvdList[i];
			}
			qtyOrdered += temp;
			System.out.println("The disc(s) have been added!");
		}
		else {
			System.out.println("The order capacity is not enough to add more!");
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		if (qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("The order capacity is not enough to add more!");
			System.out.format("Failed to add dvd(s): %s, %s\n",dvd1.getTitle(),dvd2.getTitle());
		}
		else if (qtyOrdered == MAX_NUMBERS_ORDERED - 1) {
			itemsOrdered[qtyOrdered] = dvd1;
			qtyOrdered += 1;
			System.out.println("The disc(s) have been added!");
			System.out.println("The order capacity is not enough to add more!");
			System.out.format("Failed to add dvd(s): %s, %s\n",dvd1.getTitle(),dvd2.getTitle());
		}
		else {
			itemsOrdered[qtyOrdered] = dvd1;
			qtyOrdered += 1;
			itemsOrdered[qtyOrdered] = dvd2;
			qtyOrdered += 1;
			System.out.println("The disc(s) have been added!");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc){
		int index = 0;
		int check = 0;
		for (int i = 0;i<qtyOrdered;i++){
			if (itemsOrdered[i] == disc){
				index = i;
				check = 1;
			}
		}
		if (check == 0){
			System.out.println("Disc does not exist in the order!");
			return;
		}
		for (int i = index; i<qtyOrdered - 1;i++){
			itemsOrdered[i] = itemsOrdered[i+1];
		}
		qtyOrdered -= 1;
	}
	public float totalCost(){
		float total = 0;
		for(int i = 0; i < qtyOrdered; i++){
			total += itemsOrdered[i].getCost();
		}
		return total;
	}
}