package hust.soict.ictglobal.date;
import java.time.LocalDate;
//import java.lang.*;
public class MyDate {
	private int day;
	private int month;
	private int year;
	
	public MyDate() {
		this.month = LocalDate.now().getMonthValue();
		this.year = LocalDate.now().getYear();
		this.day = LocalDate.now().getDayOfMonth();
	}
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String input) {
		String[] s = input.split(" ");
		for (int i = 0; i<s.length;i++) {
			s[i] = s[i].toLowerCase();
//			System.out.println(s[i]);
		}
//		===============================================================================
		String[] months = {"january","february","march","april","may",
				"june","july","august","september","october","november","december"};
		for (int i = 0; i<months.length;i++) {
			int x = s[0].compareTo(months[i]);

			if (x == 0) {
				this.month = i+1;
				break;
			}
		}
//		===============================================================================
		this.year = Integer.parseInt(s[2]);
//		===============================================================================
		StringBuffer temp = new StringBuffer();
		for (int i = 0; i < s[1].length();i++) {
			Boolean flag = Character.isDigit(s[1].charAt(i));
			if (flag) {
				temp.append(s[1].charAt(i));
			}
			else {
				break;
			}
		}
		this.day = Integer.parseInt(temp.toString());
	}
//	============================================
	public void printDate() {
		System.out.format("%02d-%02d-%04d\n", day, month, year);
	}
	public void printDateString() {
		System.out.format("%s %s %d\n", this.getDayString(), this.getMonthString(), year);
	}
	public int getDay() {
		return day;
	}
	public String getDayString() {
		String [] days = {"first", "second", "third", "fourth", "fifth", "sixth",  "seventh", 
				"eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", 
				"fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth",
				"twenty-first", "twenty-second", "twenty-third","twenty-fourth","twenty-fifth", 
				"twenty-sixth",	"twenty-seventh","twenty-eighth","twenty-ninth","thirtieth", "thirty-first"};
		return days[day-1];
	}
	public int getMonth() {
		return month;
	}
	public String getMonthString() {
		String [] months = {"january", "february", "march", "april", "may", "june", "july" ,
				"august", "september", "october", "november", "december"};
		return months[month - 1];
	}
	public int getYear() {
		return year;
	}
	public void setDay(int day) {
		int check = 0;
		if (this.month == 2 && this.year % 4 == 0 
				&& this.year % 100 == 0 && this.year %400 != 0 && day > 0 && day <= 28) {
			this.day = day;
			check = 1;
		}
		else {
			System.out.println("Invalid day");
			return;
		}
		if (this.month != 2) {
			if ((this.month == 1 ||this.month == 3 ||this.month == 5 ||this.month == 7 ||
					this.month == 8 ||this.month == 10 ||this.month == 12) &&(day>0 && day<=31)) {
				this.day = day;
				check = 1;
			}
			if ((this.month == 4 ||this.month == 6 ||this.month == 9 ||this.month == 11) &&(day>0 && day<=30)) {
				this.day = day;
				check = 1;
			}
		}
		if (check == 0) {
			System.out.println("Invalid day");
			return;
		}
	}
	public void setMonth(int month) {
		if (month>0 && month <= 12)
			this.month = month;
		else {
			System.out.println("Invalid month");
			return;
		}
	}
	public void setYear(int year) {
		if (year > 0 )
			this.year = year;
		else {
			System.out.println("Invalid year");
			return;
		}
	}
}
