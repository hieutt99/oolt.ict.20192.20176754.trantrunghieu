package hust.soict.ictglobal.lab02;
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
public class MergeSort{
	public static void merge(int[] array, int low, int mid, int high ){
		int[] leftArray = new int[mid-low+1];
		int[] rightArray = new int[high-mid];


		for (int i = 0; i < leftArray.length; i++){
			leftArray[i] = array[low+i];
		}
		for (int i = 0; i < rightArray.length; i++){
			rightArray[i] = array[mid+1+i];
		}
		

		int leftIndex = 0;
		int rightIndex = 0;
		for (int i = low; i < high+1;i++){
			if (leftIndex < leftArray.length && rightIndex < rightArray.length){
				if (leftArray[leftIndex] < rightArray[rightIndex]){
					array[i] = leftArray[leftIndex];
					leftIndex++;
				}
				else{
					array[i] = rightArray[rightIndex];
					rightIndex++;
				}
			}
			else if (leftIndex < leftArray.length) {
				array[i] = leftArray[leftIndex];
				leftIndex++;
			}
			else if (rightIndex < rightArray.length){
				array[i] = rightArray[rightIndex];
				rightIndex++;	
			}
			
		}
	}
	public static void sort(int[] array, int low, int high){
		// Merge Sort
		if (high <=  low) return;
		
		int mid = (low+high)/2;

		sort(array, low, mid);
		sort(array, mid + 1, high);
		merge(array, low, mid, high);
	}
	// public static void show(int[] array, int n){
	// 	for (int i = 0;i<n;i++) System.out.print(array[i]+"\t");
	// }
	public static void show(int[] array, int n){
		System.out.println(Arrays.toString(array));
	}
	public static void main(String [] args){
		int n, sum  = 0;
		Scanner input = new Scanner(System.in);
		System.out.println("Merge Sort Version");
		System.out.print("Enter no. of elements you want in array: ");
		n = input.nextInt();
		int array[] = new int[n];
		input.close();

		Random rand = new Random();
		for (int i = 0; i<n;i++){
			array[i] = rand.nextInt(100);
			sum = sum + array[i];
		}
		System.out.println("Initialized : ");
		show(array, n);

		double average = sum/n;
		
		sort(array, 0, array.length -1);
		
		System.out.println("Sorted array:");
		show(array, n);
		System.out.printf("Sum and average : %d and %.2f\n", sum, average);
	}
}
