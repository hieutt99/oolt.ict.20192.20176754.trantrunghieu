package hust.soict.ictglobal.aims.disc;
public class DigitalVideoDisc{
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;

	public String getTitle(){
		return title;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public String getCategory(){
		return category;
	}
	public void setCategory(String category){
		this.category = category;
	}
	public String getDirector(){
		return director;
	}
	public void setDirector(String director){
		this.director = director;
	}
	public int getLength(){
		return length;
	}
	public void setLength(int length){
		this.length = length;
	}
	public float getCost(){
		return cost;
	}
	public void setCost(float cost){
		this.cost = cost;
	}

	public DigitalVideoDisc(String title){
		super();
		this.title = title;
	}
	
	public boolean search (String title) {
		boolean check = true;
		String[] tokens = title.split(" ");
		String[] title_tokens = this.title.split(" ");
		
		for (int i = 0; i < tokens.length; i++) {
			int temp = 0;
			for (int j = 0; j < title_tokens.length;j++) {
				if (title_tokens[j].toLowerCase().compareTo(tokens[i].toLowerCase()) == 0) {
					temp = 1;
				}
			}
			if (temp == 0) {
				check = false;
			}
		}
		return check;
		
	}
	
}