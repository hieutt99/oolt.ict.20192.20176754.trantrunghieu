import java.util.Scanner;

public class CalculateMatrix{
	public static void main(String[] args){
		System.out.println("Input the size of matrix, the 2 matrices should be randomly initialized : ");
		Scanner input = new Scanner(System.in);

		int height = input.nextInt();
		int width = input.nextInt();
	
		System.out.println("Random matrix 1 : ");	
		Matrix m1 = new Matrix();
		m1.random(height, width);
		m1.show();

		System.out.println("Random matrix 2 : ");
		Matrix m2 = new Matrix();
		m2.random(height, width);
		m2.show();
		

		System.out.println("Result of summing 2 matrices:");
		Matrix res = m1.sum(m2);
		res.show();

	}
}
