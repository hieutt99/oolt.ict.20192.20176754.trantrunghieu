import java.util.Scanner;
import java.util.Arrays;
public class Array{
	public static void main(String [] args){
		int n, sum  = 0;
		Scanner input = new Scanner(System.in);
		System.out.print("Enter no. of elements you want in array: ");
		n = input.nextInt();
		int array[] = new int[n];
		
		for (int i = 0; i<n;i++){
			array[i] = input.nextInt();
			sum = sum + array[i];
		}
		
		double average = sum/n;
		
		for (int i = 0; i<n;i++){
			for (int j = 1;j<n-i;j++){
				if (array[j-1]>array[j]){
					int temp;
					temp = array[j];
					array[j] = array[j-1];
					array[j-1] = temp;
				}
			}
		}	
		
		
		System.out.println(Arrays.toString(array));
		System.out.printf("Sum and average : %d and %.2f\n", sum, average);
	}
}
