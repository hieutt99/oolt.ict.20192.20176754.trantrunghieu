import java.util.Scanner;

public class DaysOfMonths{

	public static void main(String args[]){
		int[] Days = {31,28,31,30,31,30,31,31,30,31,30,31};
		
		Scanner input = new Scanner(System.in);
		System.out.println("Input the month : ");
		int month = input.nextInt();
		int check = 0;
		while (check == 0){
			if (month <1 || month >12){
				System.out.println("Invalid month, Re-input:");
				month = input.nextInt();
			}
			else{
				check =1;
				break;
			}
		}
		System.out.println("Input month : "+month);
		System.out.printf("Number of month %d: %d days\n", month, Days[month-1]);
	}
}
