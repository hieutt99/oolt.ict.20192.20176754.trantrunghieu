
public class DateTest {
	public static void main(String[] args) {
		System.out.print("First Constructor : ");
		MyDate datenow = new MyDate();
		datenow.printDate();
		System.out.print("Second Constructor : ");
		MyDate datetwo = new MyDate(23,6,1999);
		datetwo.printDate();
		System.out.print("Third Constructor : ");
		MyDate date = new MyDate("February 18th 2019");
		date.printDate();
		
		
		System.out.println("Test lab 4 : ");
		System.out.format("%s %s %d\n", date.getDayString(),date.getMonthString(), date.getYear());
		MyDate dates[] = new MyDate[5];
		dates[0] = new MyDate();
		dates[1] = new MyDate(23,6,1999);
		dates[2] = new MyDate(13,6,2019);
		dates[3] = new MyDate(23,7,2010);
		dates[4] = new MyDate(23,6,2020);
		
		System.out.println("Init array :");
		for (int i = 0; i < dates.length; i++) {
			dates[i].printDate();
		}
		
		System.out.println("Test compare : ");
		int check = DateUtils.compareDate(datenow, datetwo);
		System.out.println(check);
		
		System.out.println("Sort date array :");
		dates = DateUtils.sortDate(dates);
		for (int i = 0; i < dates.length; i++) {
			dates[i].printDate();
		}
	}
}	
