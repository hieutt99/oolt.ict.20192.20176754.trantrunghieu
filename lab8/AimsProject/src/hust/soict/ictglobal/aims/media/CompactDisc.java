package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable<CompactDisc>{
	private String artist;
	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>();
//	===============================================================
	
	public CompactDisc(){
	}
	public CompactDisc(int id) {
		super(id);
	}
	public CompactDisc(String title) {
		super(title);
	}
	public CompactDisc(String title, String category) {
		super(title, category);
	}
	public CompactDisc(int id, String title, String category) {
		super(id, title, category);
	}
	public CompactDisc(int id, String title, String category, float cost) {
		super(id, title, category, cost);
	}
//	===============================================================
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public void addTrack(Track t) {
		boolean check = tracks.contains(t);
		if (check) {
			System.out.println("Element existed!");
			return;
		}
		else {
			tracks.add(t);
			System.out.println("Track added!");
		}
	}
	
	public void removeTrack(Track t) {
		boolean check = tracks.contains(t);
		if (check) {
			int pos = tracks.indexOf(t);
			tracks.remove(pos);
			System.out.println("Removed element");
		}
		else {
			System.out.println("Element does not exist in the tracklist");
			return;
		}
	}
	
	public int getLength() {
		length = 0;
		for (Track iter: tracks) {
			length += iter.getLength();
		}
		return length;
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		for (Track track: tracks) {
			track.play();
		}
	}
	public int getTrackSize() {
		return tracks.size();
	}
	
//	@Override
//	public int compareTo(CompactDisc o) {
//		// TODO Auto-generated method stub
//		int check;
//		String temp = this.getTitle();
//		check = temp.compareTo(o.getTitle());
//		return check;
//	}
	@Override
	public int compareTo(CompactDisc o) {
		// TODO Auto-generated method stub
		int this_size = this.getTrackSize();
		int o_size = o.getTrackSize();
		if (this_size > o_size) {
			return 1;
		}
		else if(this_size < o_size) {
			return -1;
		}
		else {
			int l1 = this.getLength();
			int l2 = o.getLength();
			if (l1>l2) {
				return 1;
			}
			else if (l1<l2) {
				return -1;
			}
			else {
				return 0;
			}
		}
	}
}










