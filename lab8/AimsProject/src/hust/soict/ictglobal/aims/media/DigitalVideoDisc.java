package hust.soict.ictglobal.aims.media;
public class DigitalVideoDisc extends Disc implements Playable, Comparable<DigitalVideoDisc>{
//	private String director;
//	private int length;
//	private String title;

//	=======================================================
	public DigitalVideoDisc() {	
	}
	public DigitalVideoDisc(int id) {
		super(id);
	}
	public DigitalVideoDisc(String title){
		super(title);
	}
	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}
	public DigitalVideoDisc(int id, String title, String category) {
		super(id, title, category);
	}
//	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public DigitalVideoDisc(int id, String title, String category, float cost) {
		super(id, title, category, cost);
	}
//	=========================================================
	public String getDirector(){
		return super.getDirector();
	}
	public void setDirector(String director){
		super.setDirector(director);
	}
	public int getLength(){
		return super.getLength();
	}
	public void setLength(int length){
		super.setLength(length);
	}
	public String getTitle() {
		return super.getTitle();
	}
	public void setTitle(String title) {
		super.setTitle(title);
	}
// ==================================================================
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD : " + this.getTitle());
		System.out.println("DVD Length : " + this.getLength());
	}

	
	public boolean search (String title) {
		boolean check = true;
		String[] tokens = title.split(" ");
//		String[] title_tokens = this.title.split(" ");
		String[] title_tokens = titleTokenize();
		
		for (int i = 0; i < tokens.length; i++) {
			int temp = 0;
			for (int j = 0; j < title_tokens.length;j++) {
				if (title_tokens[j].toLowerCase().compareTo(tokens[i].toLowerCase()) == 0) {
					temp = 1;
				}
			}
			if (temp == 0) {
				check = false;
			}
		}
		return check;
		
	}
	
//	@Override
//	public int compareTo(DigitalVideoDisc o) {
//		// TODO Auto-generated method stub
//		int check;
//		String temp = this.getTitle();
//		check = temp.compareTo(o.getTitle());
//		return check;
//	}
	@Override
	public int compareTo(DigitalVideoDisc o) {
		// TODO Auto-generated method stub
		float check = this.getCost() - o.getCost();
		if (check == 0) return 0;
		else if (check > 0 ) return 1; // this > o
		else return -1; //this <o
	}
	
}