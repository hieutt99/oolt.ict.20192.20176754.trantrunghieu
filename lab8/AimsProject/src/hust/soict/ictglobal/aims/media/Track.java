package hust.soict.ictglobal.aims.media;

public class Track implements Playable, Comparable<Track>{
	private String title;
	private int length;
//	=======================================================
	public Track() {
		
	}
	public Track(String title) {
		this.title = title;
	}
	public Track(String title, int length) {
		this.title = title;
		this.length = length;
	}
//	=======================================================
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD : " + this.getTitle());
		System.out.println("DVD Length : " + this.getLength());
	}
	
	@Override
	public int compareTo(Track o) {
		// TODO Auto-generated method stub
		int check;
		String temp = this.getTitle();
		check = temp.compareTo(o.getTitle());
		return check;
	}

}
