package hust.soict.ictglobal.aims.media;
import java.util.*;
public class Book extends Media implements Comparable<Book>{
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens;
	private Map<String, Integer> wordFrequency;
//  =================================================================
	public Book() {
		// TODO Auto-generated constructor stub
	}
	public Book(int id) {
		super(id);
	}
	public Book(String title){
		super(title);
	}
	public Book(String title, String category){
		super(title, category);
	}
	public Book(int id, String title, String category) {
		super(id, title, category);
	}
	public Book(int id, String title, String category, float cost) {
		super(id, title, category, cost);
	}
//	==================================================================
	public void addAuthor(String authorName) {
		boolean check = authors.contains(authorName);
		if (check) {
			System.out.println("The author has already been in the list!");
			return;
		}
		else {
			authors.add(authorName);
			System.out.println("Added author to the list");
		}
	}
	
	public void removeAuthor(String authorName) {
		boolean check = authors.contains(authorName);
		if (!check) {
			System.out.println("The author is not in the list!");
			return;
		}
		else {
			int pos = authors.indexOf(authorName);
			authors.remove(pos);
		}
	}
	public void modifyContent(String content) {
		this.content = content;
		this.processContent();
	}
	public void processContent() {
		contentTokens = Arrays.asList(content.split("\\P{L}"));
		for(String iter: contentTokens) {
			System.out.println(iter);
		}
		Collections.sort((List<String>) contentTokens);
	}
	
	@Override
	public int compareTo(Book o) {
		// TODO Auto-generated method stub
		int check;
		String temp = this.getTitle();
		check = temp.compareTo(o.getTitle());
		return check;
	}
	
}
