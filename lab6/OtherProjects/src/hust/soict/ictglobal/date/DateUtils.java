package hust.soict.ictglobal.date;
public class DateUtils {
	public static int compareDate(MyDate x, MyDate y) {
//		x<y ? 1 : -1 else x == y return 0
		if (x.getYear() < y.getYear()) {
			return 1;
		}
		else if (x.getYear() > y.getYear()){
			return -1;
		}
		else {
			if(x.getMonth() < y.getMonth()) {
				return 1;
			}
			else if (x.getMonth() > y.getMonth()) {
				return -1;
			}
			else {
				if (x.getDay() < y.getDay()) {
					return 1;
				}
				else if (x.getDay() > y.getDay()) {
					return -1;
				}
				else {
					return 0;
				}
			}
		}
		
	}
	public static MyDate[] sortDate(MyDate[] array) {
		
		for (int i = 0; i < array.length; i++) {
			for (int j = 1;j < array.length - i;j++) {
				if (compareDate(array[j - 1],array[j] ) <= 0) {
					MyDate temp = array[j-1];
					array[j-1] = array[j];
					array[j] = temp;
				}
			}
		}
		return array;
	}
}
