package hust.soict.ictglobal.garbage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class NoGarbage {
	public static void main(String[] args) throws IOException {
		File f = new File("/media/minari/0FE618020FE61802/Kcode/oop20192/lab5/OtherProjects/src/hust/soict/ictglobal/garbage/text.txt");
		if (!f.exists() && !f.canRead()) {
			System.out.println("Cant read file");
			return;
		}

		BufferedReader br = new BufferedReader(new FileReader(f));
		StringBuilder sb = new StringBuilder();
		int prevAddress = sb.hashCode();
		String line;
		int count = 0;
		
		System.out.println(prevAddress);
		
		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
			
			int curAddress = sb.hashCode();
			if (prevAddress != curAddress) {
				prevAddress = curAddress;
				System.out.println(curAddress);
				count += 1;
			}
		}
		System.out.println("Garbage: " + count);
		br.close();
	}
}
