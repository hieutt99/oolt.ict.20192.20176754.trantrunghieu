package hust.soict.ictglobal.aims.media;
import java.util.*;
public class Book extends Media{
	private List<String> authors = new ArrayList<String>();

	public Book() {
		// TODO Auto-generated constructor stub
	}
	public Book(String title){
		super(title);
	}
	public Book(String title, String category){
		super(title, category);
	}
	public Book(String title, String category, List<String> authors){
		super(title, category);
		this.authors = authors; 
		//TODO: check author condition
	} 
	public void addAuthor(String authorName) {
		boolean check = authors.contains(authorName);
		if (check) {
			System.out.println("The author has already been in the list!");
			return;
		}
		else {
			authors.add(authorName);
			System.out.println("Added author to the list");
		}
	}
	
	public void removeAuthor(String authorName) {
		boolean check = authors.contains(authorName);
		if (!check) {
			System.out.println("The author is not in the list!");
			return;
		}
		else {
			int pos = authors.indexOf(authorName);
			authors.remove(pos);
		}
	}
	
}
