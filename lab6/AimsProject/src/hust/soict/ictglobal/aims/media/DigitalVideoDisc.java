package hust.soict.ictglobal.aims.media;
public class DigitalVideoDisc extends Media{
	private String director;
	private int length;
	public String getDirector(){
		return director;
	}
	public void setDirector(String director){
		this.director = director;
	}
	public int getLength(){
		return length;
	}
	public void setLength(int length){
		this.length = length;
	}
	public DigitalVideoDisc(String title){
		super(title);
//		this.title = title;
	}
	
	public boolean search (String title) {
		boolean check = true;
		String[] tokens = title.split(" ");
//		String[] title_tokens = this.title.split(" ");
		String[] title_tokens = titleTokenize();
		
		for (int i = 0; i < tokens.length; i++) {
			int temp = 0;
			for (int j = 0; j < title_tokens.length;j++) {
				if (title_tokens[j].toLowerCase().compareTo(tokens[i].toLowerCase()) == 0) {
					temp = 1;
				}
			}
			if (temp == 0) {
				check = false;
			}
		}
		return check;
		
	}
	
}