package hust.soict.ictglobal.aims.order;
import java.time.*;
//import java.lang.*;
import java.util.*;

import hust.soict.ictglobal.aims.media.*;

public class Order{
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private ArrayList<Integer> idList = new ArrayList<Integer>();
	public static final int MAX_NUMBERS_ORDERED = 10;
	public String dateTime = new String();
	
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders = 0;
	
	
	public Order() {
		if (nbOrders < MAX_LIMITED_ORDERS) {
			nbOrders += 1;
		}
		else {
			System.out.println("Unable to create more order(s).");
			throw new IllegalArgumentException();
		}
		dateTime = LocalDate.now().toString().concat(" ").concat(LocalTime.now().toString());
	}

	public void getDateTime() {
		System.out.println("Date created : "+dateTime);
	}
	
	public void addMedia(Media in) {
//		boolean check = itemsOrdered.contains(in);
//		if (check) {
//			System.out.println("Existed element!");
//			return;
//		}
		if (itemsOrdered.size()<MAX_NUMBERS_ORDERED) { 
			itemsOrdered.add(in);
			idList.add(in.getId());
		}
		else {
			System.out.println("Not enough capacity to insert!");
			return;
		}
	}
	public void removeMedia(Media rem) {
		boolean check = itemsOrdered.contains(rem);
		if (check) {
			int pos = itemsOrdered.indexOf(rem);
			itemsOrdered.remove(pos);
			idList.remove(pos);
			System.out.println("Removed element");
		}
		
	}
	public void removeMedia(int id) {
		boolean check = idList.contains(idList.get(id));
		if (check) {
			int pos = idList.indexOf(id);
			idList.remove(pos);
			itemsOrdered.remove(pos);
			
			System.out.println("Removed element");
		}
		else {
			System.out.println("No id in the Order");
		}
		
	}
	public float totalCost() {
		float total = 0;
		for (Media iter : itemsOrdered) {
			total += iter.getCost();
		}
		return total;
	}
	public void printList() {
		System.out.println("*********************Order*************************");
		System.out.format("Date : %s\n", dateTime);
		System.out.println("Ordered Item : ");
		int i = 0;
		float total = 0;
		for (Media iter : itemsOrdered) {
			System.out.format("%d - %d.%15s - %15s: %.3f $.\n", i+1, iter.getId(),iter.getTitle(),
					iter.getCategory(), iter.getCost());
			i += 1;
			total+= iter.getCost();
		}
		System.out.println("Total cost : "+total+" $");
		System.out.println("***************************************************");
	}
	public int length() {
		return itemsOrdered.size();
	}
}