package hust.soict.ictglobal.aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.media.*;
import hust.soict.ictglobal.aims.order.Order;

public class Aims{
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	public static void main(String[] args) {
		int choice;
		Order newOrder = new Order();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		newOrder.addMedia(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		newOrder.addMedia(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
		newOrder.addMedia(dvd3);
		System.out.println(newOrder.length());
		
		Scanner scan = new Scanner(System.in);
		do {
			showMenu();
			System.out.print("Input your choice \\> ");
			choice = scan.nextInt();
			switch(choice) {
				case 1:
						newOrder = new Order();
						System.out.println(newOrder.length());
						break;
				case 2:
						System.out.print("Add item to Order : ");
						
//						newOrder
						break;
				case 3:	
						System.out.print("Delete item by id : ");
						System.out.print("Input id : ");
						int id = scan.nextInt();
						newOrder.removeMedia(id);
						break;
				case 4:
						newOrder.printList();
						break;
				case 0:
						System.out.println("Quitting!");
						break;
				default:
					System.out.println("Invalid input !");
					break;
					
			}
		}while(choice != 0);
		
		scan.close();
		return;
	}
	
	
//	public static void main(String[] args){
//		Order anOrder = new Order();
//		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
//		dvd1.setCategory("Animation");
//		dvd1.setCost(19.95f);
//		dvd1.setDirector("Roger Allers");
//		dvd1.setLength(87);
//		
//		anOrder.addDigitalVideoDisc(dvd1);
//		
//		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
//		dvd2.setCategory("Science Fiction");
//		dvd2.setCost(24.95f);
//		dvd2.setDirector("George Lucas");
//		dvd2.setLength(124);
//		
//		anOrder.addDigitalVideoDisc(dvd2);
//		
//		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
//		dvd3.setCategory("Animation");
//		dvd3.setCost(18.99f);
//		dvd3.setDirector("John Musker");
//		dvd3.setLength(90);
//		
//		anOrder.addDigitalVideoDisc(dvd3);
//
//		System.out.print("Total Cost is : ");
//		System.out.println(anOrder.totalCost());
//		System.out.print("Number of elements : ");
//		System.out.println(anOrder.getQty());
//		
//		System.out.println("Test remove elements: 1");
//		anOrder.removeDigitalVideoDisc(dvd1);
//		System.out.print("Number of elements left : ");
//		System.out.println(anOrder.getQty());
//		System.out.println("Check title : ");
//		for (int i = 0; i< anOrder.getQty();i++){
//			System.out.println(anOrder.itemsOrdered[i].getTitle());
//		}
//		
//		System.out.println("test lab 4 : ");
//		DigitalVideoDisc dvdList[] = new DigitalVideoDisc[3];
//		dvdList[0] = dvd3;
//		dvdList[1] = dvd2;
//		dvdList[2] = dvd1;
//		anOrder.addDigitalVideoDisc(dvdList);
//		System.out.print("Number of elements : ");
//		System.out.println(anOrder.getQty());
//		
//		System.out.println("continue test : ");
//		anOrder.addDigitalVideoDisc(dvdList[0],dvdList[1]);
//		System.out.print("Number of elements : ");
//		System.out.println(anOrder.getQty());
//		
//		anOrder.getDateTime();
//		anOrder.printOrder();
//	}	
}
















